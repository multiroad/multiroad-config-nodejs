### Multiroad-Config-NodeJS

Этот модуль создан для того, чтобы организовать сбор конфигурационных параметров из переменных окружения в одном месте

Для использования надо создать в корне проекта файл `multiroad.config.js` (или `.json`) в нём создать объект, ключи которого - имена конфигурационных параметров, а значения - значения по умолчанию для них

Пример:

```json
{
  "KEK": "cheburek"
}
```

Использование:

```js
const config = require('multiroad-config-nodejs');
// Или
import config from 'multiroad-config-nodejs';

console.log(config.KEK); // cheburek
```

Если будет существовать переменную окружения, имя которой совпадает с именем параметра - значение параметра будет переопределено на значение пременной окружения

```js
/**
 * Содержимое .env файла:
 * KEK=echpochmak
 */
import 'dotenv/config';
import config from 'multiroad-config-nodejs';

console.log(config.KEK); // echpochmak
```

